#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>

#define BUTTON_PIN 2
#define URL_TO_ON "http://192.168.4.1/on"
#define URL_TO_OFF "http://192.168.4.1/off"
#define UPDATE_TIMEOUT 5000
#define SSID "Ne moy wifi i tochka"
#define PASSWORD "Ma18101976"
#define FILTER_ITERATION_COUNT 10

void handle(String url)
{
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.println("WiFi Disconnected");
    return;
  }

  WiFiClient client;
  HTTPClient http;

  http.begin(client, url.c_str());

  int httpResponseCode = http.GET();

  if (httpResponseCode <= 0)
  {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
    http.end();
  }

  Serial.print("HTTP Response code: ");
  Serial.println(httpResponseCode);
  String payload = http.getString();
  Serial.println(payload);
  http.end();
}

void setup()
{
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  Serial.begin(115200);

  WiFi.begin(SSID, PASSWORD);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

bool filteredDigitalRead(uint8_t pin)
{
  static bool lastState = false;

  if (bool currentState = !digitalRead(pin); currentState == lastState)
  {
    return lastState;
  }

  for (int i = 0; i < FILTER_ITERATION_COUNT; i++)
  {
    if (bool currentState = !digitalRead(pin); currentState == lastState)
    {
      return lastState;
    }
  }

  lastState = !lastState;

  return lastState;
}

void loop()
{
  static unsigned long lastTime = 0;
  static bool lastState = false;

  if (bool currentState = filteredDigitalRead(BUTTON_PIN); currentState != lastState)
  {
    lastState = currentState;
    handle(lastState ? URL_TO_ON : URL_TO_OFF);
  }

  if ((millis() - lastTime) > UPDATE_TIMEOUT)
  {
    if (lastState)
    {
      handle(URL_TO_ON);
    }

    lastTime = millis();
  }
}